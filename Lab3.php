<!DOCTYPE html>
<html>
    <head>
        <title>SilverJack</title>
        <link rel="stylesheet" href="Lab3.css">
    </head>
    
    <body>
        <?php
        
            echo '<h2 class="title"> Silver Jack </h2>';
        
            // 0 ->51
            //clubs 0 -> 12
            //diamonds 13 -> 25
            //hearts 26 -> 38
            //spades 39 -> 51
            
            $back = 'card_back';
            $image[51];
            $isUsed[51];
            
            $name[0] = 'akame';
            $name[1] = 'tatsumi';
            $name[2] = 'lubbock';
            $name[3] = 'chelsea';
            shuffle($name);
            
            // initialize
            for ($i=0 ; $i<13 ; $i++)
            {
                $image[$i] = '<img src = "cards/clubs/'    . ($i+1) . '.png">';
                $isUsed[$i] = false;
                $image[$i+13] = '<img src = "cards/diamonds/' . ($i+1) . '.png">';
                $isUsed[$i+13] = false;
                $image[$i+26] = '<img src = "cards/hearts/'   . ($i+1) . '.png">';
                $isUsed[$i+26] = false;
                $image[$i+39] = '<img src = "cards/spades/'   . ($i+1) . '.png">';
                $isUsed[$i+39] = false;
            }
            
            //dispense
            for($i=0; $i<4; ++$i)
            {
                for($j=0; $j<6 && $score[$i] < 42; ++$j)
                {
                    $dispensed = false;
                    while(!$dispensed)
                    {
                        $rnd = rand(0,51);
                        if(!$isUsed[$rnd])
                        {
                            $score[$i] += ($rnd % 13) + 1;
                            $player[$i][$j] = $rnd;
                            $isUsed[$rnd] = true;
                            $dispensed = true;
                        }
                    }
                }
            }
            
            //echo '<table>';
            
            //score - display
            echo '<table class="main">';
            for($i=0; $i<count($player); ++$i)
            {
                echo '<tr><td><img class="char" src="character/' . $name[$i] . '.png"></td>';
                //echo $name[$i] . ' ';
                $j=0;
                while($j<count($player[$i]))
                {
                    //echo $player[$i][$j] . ' ';
                    echo '<td>' . $image[$player[$i][$j]] . '</td>';
                    ++$j;
                }
                while($j<6)
                {
                    echo '<td><img src="cards/card_back.png"></td>';
                    ++$j;
                }
                echo '<td class="score"><div class="name">' . ucfirst($name[$i]) . '</div>Total: ' . $score[$i] . '</td></tr>'; 
                //echo ' Total:' . $score[$i] .'<br>';
            }
            echo '</table>';
            
            //display Winner
            $topScore = 0;
            for($i=0; $i<4; ++$i)
            {
                if ($score[$i] < 43)
                {
                    if($score[$i] < $topScore)
                        $winner[$i] = false;
                    if($topScore < $score[$i])
                    {
                        $topScore = $score[$i];
                        for ($j=0; $j<4; ++$j)
                            $winner[$j] = false;
                        $winner[$i] = true;
                    }
                    if($topScore == $score[$i])
                        $winner[$i] = true;
                }
                else
                    $winner[$i] = false;
            }
            
            echo '<div>';
            $nbWinner = 0;
            for ($i=0; $i<4; ++$i)
            {
                if ($winner[$i])
                {
                    echo ucfirst($name[$i]). ' ';
                    ++$nbWinner;
                }
            }
            
            if ($nbWinner > 1)
            {
                echo 'are the winners !';
            }
            else 
            {
                echo 'is the winner !';
            }
            echo '</div>';
        ?>
        <a href="Lab3.php"> PLAY AGAIN </a>
    </body>
</html>    